const fs = require('fs')
const path = require('path')
const inquirer = require('inquirer')
const vorpal = require('vorpal')()
const chalk = require('chalk')
const state = require('../../state/state')
const helpers = require('../../helpers/helpers')

const selectInput = (cb) => {
    inquirer.registerPrompt('directory', require('inquirer-select-directory'))
    inquirer.prompt({
        type: 'directory',
        name: 'source',
        message: "Selection du dossier d'entrée",
        basePath: './',
        options: {
            displayFiles: false
        }
    })
    .then(function(res) {
        state.input = res.source
        helpers.printSelectedDir("d'entrée", state.input)
        listFiles(cb)
    })
}


const listFiles = (cb) => {
    fs.readdirSync(state.input).forEach(file => { 
        const filePath = `${state.input}/${file}`
        const fileExt = path.extname(file)
        state.files.push({
            path: filePath,
            ext: fileExt
        })
    }); 
    listFilesTypes(cb)
}

const listFilesTypes = (cb) => {
    let types = []
    state.files.forEach(file => {
        types.push(file.ext)
    });
    const uniqTypes = [...new Set(types)]
    state.fileTypes = uniqTypes

    if (uniqTypes.length > 1) {
        vorpal.log(uniqTypes.length + " types de fichiers ont été détéctés dans le dossier source :")
        vorpal.log(chalk.magentaBright.bold(uniqTypes.join('  ')))
        vorpal.log("Le(s) quel(s) renommé(s) ?")
        let choices = []
        uniqTypes.map(type => {
            choices.push({ name: type })
        })

        inquirer.prompt({
            type: 'checkbox',
            name : 'types',
            message: "Choisissez le(s) type(s) de fichier(s) a renommé(s)",
            choices: choices
        })
        .then((answers) => {
            answers.types.map(type => {
                state.selectedTypes.push(type)
            })
            state.files = []
            fs.readdirSync(state.input).forEach(file => { 
                const filePath = `${state.input}/${file}`
                const fileExt = path.extname(file)
                if (state.selectedTypes.includes(fileExt)) {
                    state.files.push({
                        path: filePath,
                        ext: fileExt
                    })
                }
            })
            cb()
        })
    }

}

exports.selectInput = selectInput
exports.listFiles = listFiles
exports.listFilesTypes = listFilesTypes