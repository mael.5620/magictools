const vorpal = require('vorpal')()
const fs = require('fs')
const inquirer = require('inquirer')
const cliProgress = require('cli-progress')
const state = require('../../state/state')

const validSteps = (cb) => {
    if (state.input != '' && state.output != '' && state.files != []) {
        chooseFilesNames(cb)
    } else {
        vorpal.log("Veuillez compléter les étapes précédentes.")
        cb()
    }
}

const chooseFilesNames = (cb) => {
    inquirer.prompt({
        type: 'input',
        name: 'newFilesName',
        message: 'Définir le nouveau nom des fichiers'
    })
    .then((answer) => {
        state.newName = answer.newFilesName
        renameFiles(cb)
    })
}

const renameFiles = (cb) => {
    const newPathsList = []
    const numberOfFiles = state.files.length

    state.files.map((file) => {
        let fileNum = 1
        while(fileNum <= numberOfFiles) {
            const newPath = `${state.output}/${state.newName}_${fileNum}${file.ext}`
            newPathsList.push(newPath)
            fileNum++
        }
    })
    const progressBar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic)
    progressBar.start(numberOfFiles, 0)

    state.files.map((file, index) => {
        if (state.selectedTypes.includes(file.ext)) {
            fs.renameSync(file.path, newPathsList[index])
            progressBar.increment()
            if (index + 1 == numberOfFiles) {
                progressBar.stop()
                vorpal.log("Tâche effectuée avec succès !")
                numberOfFiles > 1 ? (
                    vorpal.log(`${numberOfFiles} fichiers renommés.`)
                    ) : (
                    vorpal.log(`${numberOfFiles} fichier renommés.`)
                )
                resetState()
            }
        }
        cb()
    })
}

const resetState = () => {
    state.input = ''
    state.output = ''
    state.files = []
    state.selectedTypes = []
    state.fileTypes = []
    state.newName = ''
}

exports.validSteps = validSteps
exports.chooseFilesNames = chooseFilesNames
exports.renameFiles = renameFiles
exports.resetState = resetState