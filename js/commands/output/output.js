const fs = require('fs')
const inquirer = require('inquirer')
const helpers = require('../../helpers/helpers')
const state = require('../../state/state')

const outputPrompt = (cb) => {
    inquirer.prompt({
        type: 'confirm',
        name: 'sameDir',
        message: "Utiliser le même dossier qu'à l'entrée ?"
    })
    .then((answer) => {
        if (answer.sameDir) {
            state.output = state.input
            helpers.printSelectedDir("de sortie", state.output)
            cb()
        } else {
            selectOutput(cb)
        }
    })
}

const selectOutput = (cb) => {
    inquirer.prompt({
        type: 'confirm',
        name: 'folderExist',
        message: "Utiliser un dossier existant ?"
    })
    .then((answer) => {
        if (answer.folderExist) {
            inquirer.registerPrompt('directory', require('inquirer-select-directory'))
            inquirer.prompt({
                type: 'directory',
                name: 'output',
                message: "Selection du dossier de sortie",
                basePath: './',
                options: {
                    displayFiles: false
                }
            })
            .then(function(res) {
                state.output = res.output
                helpers.printSelectedDir("de sortie", state.output)
                cb()
            })
        } else {
            let outputFolderDir, outputFolderName

            inquirer.registerPrompt('directory', require('inquirer-select-directory'))
            inquirer.prompt({
                type: 'directory',
                name: 'dir',
                message: "Dans quel répertoire créer le dossier ?",
                basePath: './',
                options: {
                    displayFiles: false
                }
            })
            .then(function(res) {
                outputFolderDir = res.dir

                inquirer.prompt({
                    type: 'input',
                    name: 'folderName',
                    message: "Quel est le nom du dossier a créer ?",
                    default: "default"
                })
                .then((answer) => {
                    outputFolderName = answer.folderName
                    createFolder(outputFolderDir, outputFolderName)
                    helpers.printSelectedDir("de sortie", state.output)
                    cb()
                })
            })
        }
    })
}

const createFolder = (path, name) => {
    const dir = `${path}/${name}`
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir)
        state.output = dir
    }
    else {
        vorpal.log("Le dossier existe déjà et n'a pas pu être créé.")
    }
}

exports.outputPrompt = outputPrompt
exports.selectOutput = selectOutput
exports.createFolder = createFolder