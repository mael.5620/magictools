const vorpal = require('vorpal')()
const chalk = require('chalk')

const printSelectedDir = (type, path) => {
    vorpal.log(`Le dossier ${type} est : ${chalk.green(path)}`)
}

exports.printSelectedDir = printSelectedDir
