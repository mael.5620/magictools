const vorpal = require('vorpal')()
const chalk = require('chalk')
const input = require('./js/commands/input/input')
const output = require('./js/commands/output/output')
const rename = require('./js/commands/rename/rename')
const state = require('./js/state/state')


vorpal
    .delimiter('$')
    .show()
    .log('Bienvenue !')
    .log(" - Pour commencer selectionnez le dossier source  |  " + chalk.bold.green("input ou in"))
    .log(" - Ensuite selectionnez le dossier de destination  |  " + chalk.bold.green("output ou out"))
    .log(" - Enfin lancez le renommage des fichiers  |  " + chalk.bold.green("rename ou rn"))

    vorpal.command('input', 'Selection du dossier source')
    .alias('in')
    .action(function(args, cb) {
        state.input = {}
        state.files = []
        input.selectInput(cb)
    })
vorpal.command('output', 'Selection du dossier de sortie')
    .alias('out')
    .action(function(args, cb) {
        state.output = {}
        output.outputPrompt(cb)
    })
vorpal.command('rename', "Lancer le renommage des fichiers")
    .alias('rn')
    .action((args, cb) => {
        rename.validSteps(cb)
    })
vorpal.command('state', 'Affiche létat de app')
    .action(function(args, cb) {
        console.log(state)
        cb()
    })